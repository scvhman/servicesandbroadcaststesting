package apps.scvh.com.serviceandbroadcaststraining;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.util.Log;

import apps.scvh.com.serviceandbroadcaststraining.TestAsyncs.AsyncTaskOne;
import apps.scvh.com.serviceandbroadcaststraining.TestAsyncs.AsyncTaskTwo;

public class TestService extends Service {

    private AsyncTaskOne taskOne;
    private AsyncTaskTwo taskTwo;

    public TestService() {
    }

    @Override
    public void onCreate() {
        super.onCreate();
        TestingLogger logger = new TestingLogger();
        taskOne = new AsyncTaskOne(logger);
        taskTwo = new AsyncTaskTwo(logger);
    }

    @Override
    public IBinder onBind(Intent intent) {
        Log.d("service binding", "works");
        return new TestBinder();
    }

    public void testLog() {
        taskOne.execute();
        taskTwo.execute();
    }

    public class TestBinder extends Binder {
        TestService getServiceForTest() {
            return TestService.this;
        }
    }
}
