package apps.scvh.com.serviceandbroadcaststraining;

import android.os.Bundle;
import android.preference.PreferenceActivity;
import android.preference.PreferenceManager;

public class Prefs extends PreferenceActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.blank);
        PreferenceManager.setDefaultValues(this, R.xml.prefs, false);
        addPreferencesFromResource(R.xml.prefs);
    }


}
