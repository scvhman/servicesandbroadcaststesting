package apps.scvh.com.serviceandbroadcaststraining;


import android.util.Log;

public class TestingLogger {

    public synchronized void logThis(String firstArg, String secondArg) {
        Log.d(firstArg, secondArg);
    }
}
