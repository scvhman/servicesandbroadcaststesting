package apps.scvh.com.serviceandbroadcaststraining;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

public class TestReceiver extends BroadcastReceiver {

    public TestReceiver() {
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        Log.d("it works", intent.getStringExtra("test"));
    }
}
