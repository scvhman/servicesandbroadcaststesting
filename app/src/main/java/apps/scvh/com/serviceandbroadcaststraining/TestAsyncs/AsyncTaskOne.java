package apps.scvh.com.serviceandbroadcaststraining.TestAsyncs;


import android.os.AsyncTask;

import apps.scvh.com.serviceandbroadcaststraining.TestingLogger;

public class AsyncTaskOne extends AsyncTask {

    public AsyncTaskOne(TestingLogger logger) {
        this.logger = logger;
    }

    private TestingLogger logger;

    @Override
    protected Object doInBackground(Object[] params) {
        int i = 0;
        while (i < 10){
            i++;
            logger.logThis("first", "task");
        }
        return null;
    }
}
