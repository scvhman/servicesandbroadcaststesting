package apps.scvh.com.serviceandbroadcaststraining;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;

public class ActivityWithResult extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_with_result);
        findViewById(R.id.send_result).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                Bundle bundle = new Bundle();
                bundle.putString("result", String.valueOf(((EditText) findViewById(R.id.input))
                        .getText()));
                intent.putExtra("result", bundle);
                setResult(1, intent);
                finish();
            }
        });
    }

}
