package apps.scvh.com.serviceandbroadcaststraining;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private TestService testService;
    private ServiceConnection connection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            TestService.TestBinder binder = (TestService.TestBinder) service;
            testService = binder.getServiceForTest();
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            Log.d("service", "disconnected");
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        findViewById(R.id.test).setOnClickListener(this);
        findViewById(R.id.broadcasr).setOnClickListener(this);
        //leaking but it's not reakl app so i don't care
        Intent startServiceIntent = new Intent(this, TestService.class);
        bindService(startServiceIntent, connection, Context.BIND_AUTO_CREATE);
        //for fun setting it not in onclick
        findViewById(R.id.launch_for_result).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("activity for result", "sending intent");
                Intent resultIntent = new Intent(MainActivity.this, ActivityWithResult.class);
                startActivityForResult(resultIntent, 2);
            }
        });
        findViewById(R.id.launch_prefs).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent gotoPrefs = new Intent(MainActivity.this, Prefs.class);
                startActivity(gotoPrefs);
            }
        });
        showPrefs();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.test:
                testService.testLog();
                break;

            case R.id.broadcasr:
                Intent bundleIntent = new Intent();
                bundleIntent.setAction("testBroadcast");
                bundleIntent.putExtra("test", "testing broadcast");
                sendBroadcast(bundleIntent);
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.d(data.getBundleExtra("result").getString("result"), "test");
    }

    public void showPrefs() {
        SharedPreferences manager = PreferenceManager.getDefaultSharedPreferences(this);
        Log.d("first pref", manager.getString("text", ""));
        Log.d("sec pref", String.valueOf(manager.getBoolean("checkbox", false)));
    }
}
