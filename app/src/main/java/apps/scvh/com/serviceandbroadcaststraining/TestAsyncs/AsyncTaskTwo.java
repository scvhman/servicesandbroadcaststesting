package apps.scvh.com.serviceandbroadcaststraining.TestAsyncs;


import android.os.AsyncTask;

import apps.scvh.com.serviceandbroadcaststraining.TestingLogger;

public class AsyncTaskTwo extends AsyncTask {

    private TestingLogger logger;

    public AsyncTaskTwo(TestingLogger logger) {
        this.logger = logger;
    }

    @Override
    protected Object doInBackground(Object[] params) {
        int i = 0;
        while (i < 10){
            i++;
            logger.logThis("second", "task");
        }
        return null;
    }
}
